This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting started

```bash
git clone https://gitlab.com/m.khalikov/test-react-graphql.git
```

```bash
cd test-react-graphql
```

```bash
npm install
```

```bash
npm start
```

## Notice

Application uses proxy to localhost:4000 for /api requests