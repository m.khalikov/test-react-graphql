import React from 'react'
import { useDispatch } from "react-redux";
import { submit } from 'redux-form'
import { useMutation } from '@apollo/react-hooks';

import { UPDATE_USER } from '../../queries'
import classes from './Account.module.sass'
import Button from '../../components/UI/Button/Button'

import AccountForm from '../../components/Forms/AccountForm/AccountForm'
import PasswordForm from '../../components/Forms/PasswordForm/PasswordForm'

const Account = props => {

  const [update] = useMutation(UPDATE_USER, { errorPolicy: 'all' });
  const dispatch = useDispatch();
  const { session: { currentUser } } = props

  let name = currentUser.email
  if (currentUser.firstName || currentUser.secondName) name = currentUser.firstName + ' ' + currentUser.secondName

  const sumbitHandler = values => {
    let variables = {
      id: currentUser.id,
      email: currentUser.email,
      firstName: values.firstName || currentUser.firstName,
      secondName: values.secondName || currentUser.secondName,
    }
    if (values.password) variables.password = values.password

    update({ variables }).then(
      async ({ data }) => {
        await props.refetch();
        props.history.goBack();
      },
      error => console.log(error)
    );
  }

  return (
    <div className={classes.Wrapper}>
      <div className={classes.Title}>
        <h1>{name}. Редактирование</h1>
        <Button btnType="Supernova" clicked={() => dispatch(submit('account'))} width={222}>Сохранить и вернуться</Button>
      </div>
      <div className={classes.Aside}>
        <div className={classes.Navigation}>Общие данные</div>
      </div>
      <div className={classes.Content}>
        <div className={classes.Card}>
          <AccountForm onSubmit={sumbitHandler} initialValues={{ ...currentUser }}></AccountForm>
          <hr className={classes.Hr} />
          <PasswordForm onSubmit={sumbitHandler} initialValues={{ ...currentUser }} />
        </div>
      </div>
    </div>
  )
}

export default Account