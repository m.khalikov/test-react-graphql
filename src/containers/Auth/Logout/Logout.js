import React, { useEffect } from 'react';

import { useApolloClient } from "@apollo/react-hooks";


const Logout = props => {
  useEffect(() => {
    onLogout();
  }, []);

  const client = useApolloClient();

  const onLogout = async () => {
    localStorage.setItem("token", "");
    await props.refetch();
    client.resetStore();
    props.history.push("/signin")
  }

  return null

};

export default Logout