import React from 'react'
import { NavLink } from 'react-router-dom'
import { useMutation } from '@apollo/react-hooks';

import { SIGNUP_USER } from '../../../queries'
import classes from './Signup.module.sass'
import SignupForm from '../../../components/Forms/SignupForm/SingupForm'
import Error from '../../../components/UI/Error/Error'

const Signup = props => {
  const [signup, { error }] = useMutation(SIGNUP_USER, { errorPolicy: 'all' });

  const sumbitHandler = ({ email, password }) => {
    signup({ variables: { email, password } }).then(
      async ({ data }) => {
        localStorage.setItem("token", `Bearer ${data.signup}`);
        await props.refetch();
        props.history.push("/");
      },
      error => console.log(error)
    );
  }

  return (
    <div className={classes.Wrapper}>
      <p className={classes.Title}>Заполните данные для новой учетной записи</p>
      <SignupForm onSubmit={sumbitHandler} />
      <NavLink to="/signin">
        <p className={classes.Switch}>
          Уже зарегистрированы?<b> Войти</b>
        </p>
      </NavLink>
      {error && <Error error={error} />}
    </div>
  )
}

export default Signup