import React from 'react'
import { NavLink } from 'react-router-dom'
import { useMutation } from '@apollo/react-hooks';

import { SIGNIN_USER } from '../../../queries'
import classes from './Signin.module.sass'
import SigninForm from '../../../components/Forms/SigninForm/SigninForm'
import Error from '../../../components/UI/Error/Error'

const Signin = props => {
  const [signin, { error }] = useMutation(SIGNIN_USER, { errorPolicy: 'all' });

  const sumbitHandler = variables => {
    signin({ variables }).then(
      async ({ data }) => {
        localStorage.setItem("token", `Bearer ${data.login.token}`);
        await props.refetch();
        props.history.push("/");
      },
      error => console.log(error)
    );
  }
  return (
    <div className={classes.Wrapper}>
      <SigninForm onSubmit={sumbitHandler} />
      <NavLink to="/signup">
        <p className={classes.Switch}>
          Нет аккаунта?<b> Создать</b>
        </p>
      </NavLink>
      {error && <Error error={error} />}
    </div>

  )
}

export default Signin
