import React from 'react'

import { ReactComponent as Processes } from '../../../assets/processes.svg'

const Icon = props => {

  switch (props.name) {
    case 'processes':
      return <Processes />
    default:
      return null
  }

}
export default Icon