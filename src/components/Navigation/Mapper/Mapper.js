import React from 'react'

import Icon from './Icon'
import classes from './Mapper.module.sass'

const Mapper = props => {
  const { img, title } = props.mapper

  return (
    <div className={classes.Mapper}>
      <Icon name={img} />
      <span>{title}</span>
    </div>
  )
}

export default Mapper
