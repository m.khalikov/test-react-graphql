import React from 'react';

import classes from './DrawerToggle.module.sass';
import Icon from '../../../../assets/menu.svg'

const drawerToggle = (props) => (
    <div className={classes.DrawerToggle} onClick={props.clicked}>
        <img src={Icon} alt="Меню" />
        <span>Меню</span>
    </div>
);

export default drawerToggle;