import React from 'react'
import { Field, reduxForm } from 'redux-form'

import validate from '../../../shared/validate'
import Input from '../../../components/UI/Input/Input'

const AccountForm = props => (
  <>
    <Field name="firstName" type="text" component={Input} label="Имя" withLabel />
    <Field name="secondName" type="text" component={Input} label="Фамилия" withLabel />
  </>
)


export default reduxForm({
  form: 'account',
  validate,
})(AccountForm)