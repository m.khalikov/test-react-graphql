import React from 'react'
import appLogo from '../../assets/logo.svg'
import classes from './Logo.module.sass'

const logo = () => {
  return (
    <div className={classes.Logo}>
      <img src={appLogo} alt="Proceset" />
    </div>
  )
}

export default logo