import React from 'react'

import ProcessCard from './ProcessCard/ProcessCard'
import classes from './ProcessList.module.sass'

const ProcessList = props => (
  <div className={classes.Wrapper}>
    {props.processes.map(item => (
      <ProcessCard key={item.id} {...item}></ProcessCard>
    ))}
  </div>
)

export default ProcessList