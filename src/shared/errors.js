const errors = {
  "This email is already registered": "Этот адрес уже используется",
  "No user with that email": "Пользователь не найден",
  "Incorrect password": "Неправильная почта или пароль",
}
const formatError = error => errors[error] || "Неизвестная ошибка"

export default formatError