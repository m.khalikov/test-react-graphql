import time from '../assets/time.svg'
import averageTime from '../assets/average-time.svg'
import members from '../assets/members.svg'
import script from '../assets/script.svg'
import reload from '../assets/reload.svg'

const icons = {
  time,
  averageTime,
  members,
  script,
  reload,
}
export default icons